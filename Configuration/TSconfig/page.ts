# Set Editor preferences
RTE.config.tt_content.bodytext.preset = marktfinder_preset

tx_gridelements {
  excludeLayoutIds = slider,tabsSimple,tabs4,tabs6,accordion,slider
}

##id storagepage of gridlayout
TCEFORM.pages.backend_layout.PAGE_TSCONFIG_ID = 20
TCEFORM.pages.backend_layout_next_level.PAGE_TSCONFIG_ID = 20

TCEFORM.tt_content.layout.altLabels.1 = runde Bilder
TCEFORM.tt_content.layout.removeItems = 2,3

# Image orientation setup
TCEFORM.tt_content.imageorient {
  removeItems = 1,2,9,10,17,18,25,26
  altLabels.0 = oben
  altLabels.8 = unten
}
# Image columns setup
TCEFORM.tt_content.imagecols {
  removeItems = 5,6,7,8
}
TCAdefaults.tt_content.imagecols = 1



mod.web_layout.BackendLayouts {
  start {
    title = Startseite
    description = Startseite mit speziellen Eigenschaften wie Logo und Claim...
    #icon = EXT:marktfinder_template/Resources/Public/Images/BackendLayouts/start.gif
    config {
      backend_layout {
        colCount = 4
        rowCount = 4
        rows {
          1 {
            columns {
              1 {
                name = Stage
                colspan = 4
                colPos = 11
                allowed {
                  CType = mask_stageimage,shortcut
                }
              }
            }
          }
          2 {
            columns {
              1 {
                name = Inhalt
                colspan = 4
                colPos = 12
                allowed {
                  CType = header,textmedia,shortcut,list,div,html,gridelements_pi1,mask_spacer
                }
              }
            }
          }
          3 {
            columns {
              1 {
                name = Footer links (vererbt)
                colPos = 21
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
              2 {
                name = Footer mitte links (vererbt)
                colPos = 22
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
              3 {
                name = Footer mitte rechts (vererbt)
                colPos = 23
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
              4 {
                name = Footer rechts (vererbt)
                colPos = 24
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
            }
          }
        }
      }
    }
  }
  default {
    title = Standardseite
    #icon = EXT:marktfinder_template/Resources/Public/Images/BackendLayouts/default.gif
    config {
      backend_layout {
        colCount = 4
        rowCount = 4
        rows {
          1 {
            columns {
              1 {
                name = Stage
                colspan = 4
                rowspan = 1
                colPos = 11
                allowed {
                  CType = list,mask_stageimage,shortcut
                }
              }
            }
          }
          2 {
            columns {
              1 {
                name = Inhalt
                colspan = 4
                colPos = 12
                allowed {
                  CType = header,textmedia,shortcut,list,div,html,gridelements_pi1,mask_spacer
                }
              }
            }
          }
          3 {
            columns {
              1 {
                name = Footer links (vererbt)
                colPos = 21
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
              2 {
                name = Footer mitte links (vererbt)
                colPos = 22
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
              3 {
                name = Footer mitte rechts (vererbt)
                colPos = 23
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
              4 {
                name = Footer rechts (vererbt)
                colPos = 24
                allowed {
                  CType = header,textmedia,shortcut,html
                }
              }
            }
          }
        }
      }
    }
  }
}


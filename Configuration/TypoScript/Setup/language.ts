#Standardsprache festlegen
config {
    linkVars = L(0-1)
    sys_language_uid = 0
    language = {$marktfinder_template.language}
    locale_all = {$marktfinder_template.language.key}
}

# [31 in tree.rootLineIds]

#     lib.quickform {
#         20.select {

#             pidInList = 34
#             uidInList = 370
#         }
#     }

# [END]

# [53 in tree.rootLineIds]

#     lib.quickform {
#         20.select {

#             pidInList = 56
#             uidInList = 573
#         }
#     }

# [END]

marktfinder_template {
    language = de
    language.key = de_DE

    text {
        quickform.toggle = Schnell-Anfrage <span class="closed">öffnen</span><span class="open">schließen</span>
    }
}

# [31 in tree.rootLineIds]

#     marktfinder_template {
#         language = en
#         language.key = en_UK

#         text {
#             quickform.toggle = <span class="closed">Open</span><span class="open">Close</span> quick request
#         }

#         quickformUid = 370
#     }

# [END]

# [53 in tree.rootLineIds]
#     marktfinder_template {
#         language = fr
#         language.key = fr_FR

#         text {
#             quickform.toggle = <span class="closed">Ouvrir</span><span class="open">Fermer</span> l'enquête rapid
#         }

#         quickformUid = 573
#     }
# [END]

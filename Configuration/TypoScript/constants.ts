marktfinder_template {
    site {
        #domain = 9.local:8890
        #domain = www.marktfinder.de

        logoFile = EXT:marktfinder_template/Resources/Public/Images/logo.png
        name = Marktfinder
    }

    pageIds {
        root = 1

        rootDE = 1
        # rootEN = 31
        # rootFR = 53

        metaNaviRoot = 3

        quickform = 12
        contact = 5
    }

    quickformUid = 33
}


/**
* Activate lightbox
*
*/
styles.content.textmedia.linkWrap.lightboxEnabled = 1

/**
* Fluid Styled Content
*
*/
styles.templates.layoutRootPath = EXT:marktfinder_template/Resources/Extensions/fsc/Layouts
styles.templates.templateRootPath = EXT:marktfinder_template/Resources/Extensions/fsc/Templates
styles.templates.partialRootPath = EXT:marktfinder_template/Resources/Extensions/fsc/Partials

<INCLUDE_TYPOSCRIPT: source="DIR:EXT:marktfinder_template/Configuration/TypoScript/Constants/" extension="ts">
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:marktfinder_template/Configuration/TypoScript/Extensions/" extension="tsconst">


/**
* Cs_seo
* https://docs.typo3.org/typo3cms/extensions/cs_seo/Developer/ExtendSitemap/Index.html#extend-sitemap
*/
plugin.tx_csseo.sitemap.pages.rootPid = 27

plugin.tx_pxacookiebar.settings.position = bottom
plugin.tx_pxacookiebar.view.templateRootPath = EXT:marktfinder_template/Resources/Extensions/pxa_cookie_bar/Templates
plugin.tx_pxacookiebar.view.partialRootPath = EXT:marktfinder_template/Resources/Extensions/pxa_cookie_bar/Partials
plugin.tx_pxacookiebar.view.layoutRootPath = EXT:marktfinder_template/Resources/Extensions/pxa_cookie_bar/Layouts

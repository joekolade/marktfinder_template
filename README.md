# Martkfinder Template

## TYPO3 9.5

### Installation

composer.json (TYPO3):

```
{
  "repositories": [
    [other repositories],
        {
          "type": "vcs",
          "url": "public/typo3conf/ext/marktfinder_template/"
        }
```
```
{
  "repositories": [
    [other repositories],
        {
          "type": "vcs",
          "url": "git@bitbucket:joekolade/marktfinder_template/"
        }
```

dann `composer require joekolade/marktfinder_template dev-master`

### Setup

Im Root Template fsc und marktfinder_template statisch importieren

Mask umbiegen:

```
typo3conf/ext/marktfinder_template/Resources/Extensions/mask/mask.json
```

als Extconf

```
'mask' => 'a:9:{s:4:"json";s:70:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/mask.json";s:18:"backendlayout_pids";s:3:"0,1";s:7:"content";s:90:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/frontend/Templates/";s:7:"layouts";s:88:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/frontend/Layouts/";s:8:"partials";s:89:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/frontend/Partials/";s:7:"backend";s:89:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/backend/Templates/";s:15:"layouts_backend";s:87:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/backend/Layouts/";s:16:"partials_backend";s:88:"typo3conf/ext/marktfinder_template/Resources/Extensions/mask/backend/Partials/";s:7:"preview";s:57:"typo3conf/ext/marktfinder_template/Resources/Public/Mask/";}',
```
